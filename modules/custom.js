/*
  @Author : Fractalink Design Studio
  @Project : Project Name
  @Dev : Dev Name
  @Date : Date;
*/
//hpme page banner animation
function bannerEleAni() {
    var t0 = new TimelineMax({});
    var t1 = new TimelineMax({});
    var t2 = new TimelineMax({});
    var t3 = new TimelineMax({});
    var t4 = new TimelineMax({});
    var t5 = new TimelineMax({});
    var t6 = new TimelineMax({});
    var t7 = new TimelineMax({});
    var t8 = new TimelineMax({});

    var eleC1 = $(".bs-banner .wrap1");
    var eleL1 = $(".bs-banner .wrap1 .ele-lbl");

    var eleC2 = $(".bs-banner .wrap2");
    var eleL2 = $(".bs-banner .wrap2 .ele-lbl");

    var eleL3 = $(".bs-banner .wrap3");
    var eleC3 = $(".bs-banner .wrap4");
    var eleL4 = $(".bs-banner .wrap5");
    var screen1 = $(".screen.bg-one");
    var screen2 = $(".screen.bg-two");



    var masterTimeLine = new TimelineMax({});

    $(eleL4).css("opacity", 1);
    $(eleC3).css("opacity", 1);
    $(screen2).css("opacity", 1);
    t1.to(eleL1, 1.2, { opacity: 0, scale: .1, left: "-50%" }, "1.0");
    //t1.to(eleL1, 1.2, { opacity: 0 }, "1.0");
    t2.to(eleC1, 1.2, { opacity: 1, scale: 1 }, "1.0")
        .to(eleC1, 1.2, { opacity: 1, scale: 1, left: "50%", marginLeft: "-55px" }, "-=0.3");
    t3.to(eleL2, 1.2, { opacity: 0, scale: .1, left: "-50%" }, "1")
        //t3.to(eleL2, 1.2, { opacity: 0 }, "1")
        //t4.to(eleC2, 1.2, { opacity: 1, scale: 1, right: "-10%" }, "1");
    t5.to(eleC2, 1.2, { opacity: 1, scale: 1, right: "50%", marginRight: "-262px" }, "-=0.3")
        .from(eleC3, 1.2, { opacity: 0, scale: 1, rotation: 60 }, "+=.1");
    t6.to(eleL3, 1.2, { opacity: 0, scale: .1 }, "1.0");
    t7.from(eleL4, 1.2, { opacity: 0, scale: .1 }, "1.0");
    t8.from(screen2, 1.2, { left: "100%" });

    //t2.delay(1);
    // t5.delay(2);
    // t6.delay(1.5);
    // t7.delay(2);
    // t8.delay(6);
    masterTimeLine.add(t1, 0).add(t3, 0).add(t2, 0).add(t5, 2).add(t6, 2).add(t7, 2).add(t8, 6);
    masterTimeLine.timeScale(.5)
        //  $(".btn.pull-left,.btn.pull-right").css('opacity', '1');

    // console.log("length: " + $('.slide.active').length)

    /*$(".lyt-button").each(function() {
        var cSlide = $(this);

        $(".btn.pull-left,.btn.pull-right").css('opacity', '1');


        $(cSlide).find(".btn.pull-left").each(function(e) {
            var obj = this;
            t2.from(obj, 1.2, { opacity: 0, right: "100%", left: "0%" }, "-=0.3");
        });
        $(cSlide).find(".btn.pull-right").each(function(e) {
            var obj = this;
            t3.from(obj, 1.2, { opacity: 0, left: "100%", right: "0%" }, "-=0.3");
        });
        masterTimeLine.add(t1, 0).add(t2, .2).add(t3, .2);
        masterTimeLine.timeScale(3)
        timelineArr.push(masterTimeLine);
        masterTimeLine.play();

    })*/

}

function bannerEleAniMobile() {
    var t0 = new TimelineMax({});
    var t1 = new TimelineMax({});
    var t2 = new TimelineMax({});
    var t3 = new TimelineMax({});
    var t4 = new TimelineMax({});
    var t5 = new TimelineMax({});
    var t6 = new TimelineMax({});
    var t7 = new TimelineMax({});
    var t8 = new TimelineMax({});

    var eleC1 = $(".bs-banner .wrap1");
    var eleL1 = $(".bs-banner .wrap1 .ele-lbl");

    var eleC2 = $(".bs-banner .wrap2");
    var eleL2 = $(".bs-banner .wrap2 .ele-lbl");

    var eleL3 = $(".bs-banner .wrap3");
    var eleC3 = $(".bs-banner .wrap4");
    var eleL4 = $(".bs-banner .wrap5");
    var screen1 = $(".screen.bg-one");
    var screen2 = $(".screen.bg-two");



    var masterTimeLine = new TimelineMax({});

    $(eleL4).css("opacity", 1);
    $(eleC3).css("opacity", 1);
    $(screen2).css("opacity", 1);
    t1.to(eleL1, 1.2, { opacity: 0, scale: .1, left: "-50%" }, "1.0");
    //t1.to(eleL1, 1.2, { opacity: 0 }, "1.0");
    t2.to(eleC1, 1.2, { opacity: 1, scale: 1 }, "1.0")
        .to(eleC1, 1.2, { opacity: 1, scale: 1, left: "50%", marginLeft: "-23px" }, "-=0.3");
    t3.to(eleL2, 1.2, { opacity: 0, scale: .1, left: "-50%" }, "1")
        //t3.to(eleL2, 1.2, { opacity: 0 }, "1")
        //t4.to(eleC2, 1.2, { opacity: 1, scale: 1, right: "-10%" }, "1");
    t5.to(eleC2, 1.2, { opacity: 1, scale: 1, right: "50%", marginRight: "-89px" }, "-=0.3")
        .from(eleC3, 1.2, { opacity: 0, scale: 1, rotation: 60 }, "+=.1");
    t6.to(eleL3, 1.2, { opacity: 0, scale: .1 }, "1.0");
    t7.from(eleL4, 1.2, { opacity: 0, scale: .1 }, "1.0");
    t8.from(screen2, 1.2, { left: "100%" });

    //t2.delay(1);
    // t5.delay(2);
    // t6.delay(1.5);
    // t7.delay(2);
    // t8.delay(6);
    masterTimeLine.add(t1, 0).add(t3, 0).add(t2, 0).add(t5, 2).add(t6, 2).add(t7, 2).add(t8, 6);
    masterTimeLine.timeScale(.8)
        //  $(".btn.pull-left,.btn.pull-right").css('opacity', '1');

    // console.log("length: " + $('.slide.active').length)

    /*$(".lyt-button").each(function() {
        var cSlide = $(this);

        $(".btn.pull-left,.btn.pull-right").css('opacity', '1');


        $(cSlide).find(".btn.pull-left").each(function(e) {
            var obj = this;
            t2.from(obj, 1.2, { opacity: 0, right: "100%", left: "0%" }, "-=0.3");
        });
        $(cSlide).find(".btn.pull-right").each(function(e) {
            var obj = this;
            t3.from(obj, 1.2, { opacity: 0, left: "100%", right: "0%" }, "-=0.3");
        });
        masterTimeLine.add(t1, 0).add(t2, .2).add(t3, .2);
        masterTimeLine.timeScale(3)
        timelineArr.push(masterTimeLine);
        masterTimeLine.play();

    })*/

}

function bannerEleAniTablet() {
    var t0 = new TimelineMax({});
    var t1 = new TimelineMax({});
    var t2 = new TimelineMax({});
    var t3 = new TimelineMax({});
    var t4 = new TimelineMax({});
    var t5 = new TimelineMax({});
    var t6 = new TimelineMax({});
    var t7 = new TimelineMax({});
    var t8 = new TimelineMax({});

    var eleC1 = $(".bs-banner .wrap1");
    var eleL1 = $(".bs-banner .wrap1 .ele-lbl");

    var eleC2 = $(".bs-banner .wrap2");
    var eleL2 = $(".bs-banner .wrap2 .ele-lbl");

    var eleL3 = $(".bs-banner .wrap3");
    var eleC3 = $(".bs-banner .wrap4");
    var eleL4 = $(".bs-banner .wrap5");
    var screen1 = $(".screen.bg-one");
    var screen2 = $(".screen.bg-two");



    var masterTimeLine = new TimelineMax({});

    $(eleL4).css("opacity", 1);
    $(eleC3).css("opacity", 1);
    $(screen2).css("opacity", 1);
    t1.to(eleL1, 1.2, { opacity: 0, scale: .1, left: "-50%" }, "1.0");
    //t1.to(eleL1, 1.2, { opacity: 0 }, "1.0");
    t2.to(eleC1, 1.2, { opacity: 1, scale: 1 }, "1.0")
        .to(eleC1, 1.2, { opacity: 1, scale: 1, left: "50%", marginLeft: "-22px" }, "-=0.3");
    t3.to(eleL2, 1.2, { opacity: 0, scale: .1, left: "-50%" }, "1")
        //t3.to(eleL2, 1.2, { opacity: 0 }, "1")
        //t4.to(eleC2, 1.2, { opacity: 1, scale: 1, right: "-10%" }, "1");
    t5.to(eleC2, 1.2, { opacity: 1, scale: 1, right: "50%", marginRight: "-199px" }, "-=0.3")
        .from(eleC3, 1.2, { opacity: 0, scale: 1, rotation: 60 }, "+=.1");
    t6.to(eleL3, 1.2, { opacity: 0, scale: .1 }, "1.0");
    t7.from(eleL4, 1.2, { opacity: 0, scale: .1 }, "1.0");
    t8.from(screen2, 1.2, { left: "100%" });

    //t2.delay(1);
    // t5.delay(2);
    // t6.delay(1.5);
    // t7.delay(2);
    // t8.delay(6);
    masterTimeLine.add(t1, 0).add(t3, 0).add(t2, 0).add(t5, 2).add(t6, 2).add(t7, 2).add(t8, 6);
    masterTimeLine.timeScale(.8)
        //  $(".btn.pull-left,.btn.pull-right").css('opacity', '1');

    // console.log("length: " + $('.slide.active').length)

    /*$(".lyt-button").each(function() {
        var cSlide = $(this);

        $(".btn.pull-left,.btn.pull-right").css('opacity', '1');


        $(cSlide).find(".btn.pull-left").each(function(e) {
            var obj = this;
            t2.from(obj, 1.2, { opacity: 0, right: "100%", left: "0%" }, "-=0.3");
        });
        $(cSlide).find(".btn.pull-right").each(function(e) {
            var obj = this;
            t3.from(obj, 1.2, { opacity: 0, left: "100%", right: "0%" }, "-=0.3");
        });
        masterTimeLine.add(t1, 0).add(t2, .2).add(t3, .2);
        masterTimeLine.timeScale(3)
        timelineArr.push(masterTimeLine);
        masterTimeLine.play();

    })*/

}

function inputAnimation() {

    $(".form-control").each(function() {
        if ($(this).val().length > 0) {
            $(this).parents('.form-group,.input-group').addClass('active');
        }
    });

    $(".form-control").focus(function() {
        $(this).parents('.form-group,.input-group').addClass('active');
    });

    $(".form-control").blur(function() {
        console.log("blur")
        console.log($(this).val().length);
        if ($(this).val().length > 0) {
            $(this).parents('.form-group,.input-group').addClass('active');
        } else {
            $(this).parents('.form-group,.input-group').removeClass('active');
        }
    });

    $('.form-group').find('select option').each(function() {
        if ($(this).is(':selected')) {
            $(this).parents('.form-group').addClass('active');
        }
    });


    // $('.datePicker').datetimepicker().on('hide', function(ev) {
    //     if ($(this).find(".form-control").val().length > 0) {
    //         $(this).parent().addClass('active');
    //     } else {
    //         $(this).parents().removeClass('active');
    //     }
    // });
}

/* Document Ready */

function requestDemoScrollFix() {
    // var tempArr = [];
    // $(".js-sec-scroll-link").find(".link").each(function(){
    //     var idName = $(this).attr("href");
    //     var topPos = $(idName).offset().top;
    //     var eleHeight = topPos + $(idName).height();
    //     var obj = {
    //         "id":idName,
    //         "topPos":topPos,
    //         "eleHeight":eleHeight
    //     }
    //     tempArr.push(obj)
    // })

    // console.log(tempArr)
    if ($(".bs-sec.typ-head-sec").length != 0)
        var headerTop = $(".bs-sec.typ-head-sec").offset().top;
    $(window).scroll(function() {

        //console.log($(window).scrollTop())
        var topPos = $(window).scrollTop();
        var winH = $(window).height();
        //console.log("scroll pos: "+topPos);

        var headerHeight = $(".bs-header").height();
        var jsHeadHeight = headerHeight + $(".bs-sec.typ-head-sec").height() + parseInt($(".bs-sec.typ-head-sec").css("padding-top")) + parseInt($(".bs-sec.typ-head-sec").css("padding-bottom"));

        //auto highlightd sec scroll link
        $(".js-sec-scroll-link").find(".link").each(function() {
                var idName = $(this).attr("href");
                var eleTopPos = $(idName).offset().top - jsHeadHeight;
                var eleHeight = topPos + $(idName).height();
                if (topPos > eleTopPos && topPos < eleHeight) {
                    $(".js-sec-scroll-link .link").removeClass("active")
                    $(this).addClass("active")
                } else {

                    $(this).removeClass("active")
                }

            })
            // if ($('.js-home').length != 0) {} else {
            //     if (topPos > 44) {
            //         $(".btn-request-demo").addClass("dark-theme");
            //     } else {
            //         $(".btn-request-demo").removeClass("dark-theme");
            //     }
            // }

        if (topPos > 102) {
            // alert(1);
            // $(".btn-request-demo").addClass("fix-top");
            $(".btn-login").addClass('hide');
        } else {
            // $(".btn-request-demo").removeClass("fix-top");
            $(".btn-login").removeClass('hide');
        }
        if ($('.bs-sec.typ-head-sec').hasClass('js-head-sec') == true) {
            // var headerHeight = $(".bs-header").height();
            // var jsHeadHeight = headerHeight + $(".bs-sec.typ-head-sec").height()+parseInt($(".bs-sec.typ-head-sec").css("padding-top"))+parseInt($(".bs-sec.typ-head-sec").css("padding-bottom"));
            var secIndx = $(".bs-sec").index($(".bs-sec.typ-head-sec"))
                // if ($('.mobile').length == 0) {
            if (topPos > headerTop - 150) {
                $(".bs-sec.typ-head-sec").addClass("sticky");
                $(".bs-sec").eq(secIndx + 1).css({
                    "margin-top": jsHeadHeight
                })
            } else {
                $(".bs-sec.typ-head-sec").removeClass("sticky");
                $(".bs-sec").eq(secIndx + 1).css({
                    "margin-top": ""
                })
            }
            //}
            // if ($('.mobile').length != 0) {
            //     if (topPos > headerTop) {
            //         $(".bs-sec.typ-head-sec").addClass("sticky");
            //         $(".bs-sec.typ-triangle").addClass("sticky-spacer");
            //     } else {
            //         $(".bs-sec.typ-head-sec").removeClass("sticky");
            //         $(".bs-sec.typ-triangle").removeClass("sticky-spacer");
            //     }
            // }
        }



    })
}

function btnClassChange() {
    $('.bs-sec.typ-triangle .btn').removeClass('btn-default').addClass('btn-white-overlay');
}

function scrollToSection() {
    $(".js-sec-scroll-link .link").on("click", function() {
        $(".js-sec-scroll-link .link").removeClass('active');
        $(this).addClass('active');
        var headerHeight = $(".bs-header").height();
        var jsHeadHeight = headerHeight + $(".bs-sec.typ-head-sec").height() + parseInt($(".bs-sec.typ-head-sec").css("padding-top")) + parseInt($(".bs-sec.typ-head-sec").css("padding-bottom"));
        // console.log(containerH);
        var targetSec = $(this).attr("href");
        var scrollAmount = $(targetSec).offset().top - (jsHeadHeight - 20)
        console.log(scrollAmount)
            // console.log(containerH + 'container');
        $('html, body').animate({
            scrollTop: scrollAmount
        }, 1000)
        return false;

    })
    if ($(".js-sec-scroll-dropdn").length != 0) {
        $(".js-sec-scroll-dropdn").change(function(e) {
            var indxVal = $(this).val();
            $(".js-sec-scroll-link .link").eq(indxVal).trigger("click")
        })
    }
}

function videoCall() {
    $('.cust-img-wrap.typ-video-bajaj').on("click", function() {
        $('<iframe width="100%" height="100%" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/mqk8bv9UO_w?autoplay=1"></iframe>').appendTo(".mod-video");
    });
    $('.cust-img-wrap.typ-video-axis').on("click", function() {
        $('<iframe width="100%" height="100%" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/CwjbtyNxzF8?autoplay=1"></iframe>').appendTo(".mod-video");
    });
    $('.cust-img-wrap.typ-video-big-fm').on("click", function() {
        $('<iframe width="100%" height="100%" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/0OIxF9WdkVc?autoplay=1"></iframe>').appendTo(".mod-video");
    });
    $('.customer-video-modal').on('hidden.bs.modal', function(e) {
        $('iframe').remove();
    });
}

function slider() {
    var swiper = new Swiper('.bs-banner.typ-customers .swiper-container', {
        effect: 'slide',
        slidesPerView: 1,
        pagination: '.bs-banner.typ-customers .swiper-pagination',
        nextButton: '.bs-banner.typ-customers .swiper-button-next',
        prevButton: '.bs-banner.typ-customers .swiper-button-prev',
        paginationClickable: true,
        autoHeight: true,
        breakpoints: {
            768: {
                slidesPerView: 1
            },
            1024: {
                slidesPerView: 1
            }
        }
    });
}

function slider2() {
    var swiper = new Swiper('.bs-banner.typ-events .swiper-container', {
        effect: 'slide',
        slidesPerView: 1,
        pagination: '.bs-banner.typ-events .swiper-pagination',
        nextButton: '.bs-banner.typ-events .swiper-button-next',
        prevButton: '.bs-banner.typ-events .swiper-button-prev',
        paginationClickable: true,
        autoHeight: true,
        breakpoints: {
            768: {
                slidesPerView: 1
            },
            1024: {
                slidesPerView: 1
            }
        }
    });
}

function trimMonth() {
    var month = $('.month').text();
    $('.month').each(function() {
        month = $(this).text().slice(0, 3);
        $(this).html(month);
    });
}



function seoSlider() {
    var swiper = new Swiper('.mod-seo-content .swiper-container', {
        effect: 'slide',
        slidesPerView: 1,
        pagination: '.mod-seo-content .swiper-pagination',
        paginationClickable: true
    });
}

function mainNav() {
    if ($(window).width() > 1025) {
        $(".bs-main-menu .left-nav-active .item").hover(function() {
            // $(".bs-main-menu .left-nav-active .item").removeClass("active")
            $(this).parents(".left-nav-active").find(".item").removeClass("active");

            $(this).addClass("active")
        })
    } else {
        $(".bs-main-menu .item").removeClass("active");
        $(".hamburger-link").on("click", function(e) {

            if ($(this).hasClass("active") == true) {
                $(".bs-main-menu .item").removeClass("active");
                $(this).removeClass("active");
                $(".bs-main-menu").removeClass("active");
            } else {
                $(this).addClass("active");
                $(".bs-main-menu").addClass("active");
            }
        })
        $(".bs-main-menu >.item .main-link").on("click", function() {
            //e.preventDefault();
            console.log("1st")
            if ($(this).parents(".item").find(".sub-menu-wrap").length != 0) {
                console.log("1st inside")
                if ($(this).parents(".item").hasClass("active") == true) {
                    $(this).parents(".item").removeClass("active")
                } else {
                    $(this).parents(".item").addClass("active")
                }
            }

            //return false;
        })
        $(".bs-main-menu >.item  .sub-menu >.item .link").on("click", function() {
            console.log("last")
                // console.log($(this).find("a").attr("href"))
            if ($(this).parent(".item").hasClass("active") == true) {
                $(this).parent(".item").removeClass("active")
            } else {
                $(this).parent(".item").addClass("active")
            }
            //return false;
        })
    }

}

function tabHomeBanner() {
    var winH = $(window).height();
    var showH = (winH / 100) * 50;
    $('.bs-banner.typ-home').css('height', 380);
}

// function eqHeightPartner() {
//     for(i=0;i<$(".bs-info-box.typ-partner.typ-inner").length;i+2) {
//         maxHeight = $(this).height();
//         for(j=i+1;j<=i+1;j++) {
//             if($(this).height()>=maxHeight) {
//                 maxHeight = $(this).height();
//             }
//         }
//     }
// }


function connectorsSameHeight() {
    var maxItem = 0;
    var itemLen = 0;
    $('.lyt-connectors > li').each(function() {
        var itemLen = $(this).find('.connector-list-wrap .item').length;
        if (itemLen > maxItem) {
            maxItem = itemLen;
        }
    })
    $('.lyt-connectors > li').each(function() {
        var itemLen = $(this).find('.connector-list-wrap .item').length;
        var appendItem = maxItem - itemLen;
        for (var i = 0; i < appendItem; i++) {
            $(this).find('.connector-list-wrap').append("<li class='item'>&nbsp;</li>")
        }
    })
}

$(function() {
    mainNav()
    $("a").each(function() {
        if ($(this).attr('href') == '#' || $(this).attr('href') == ' ') {
            $(this).attr('href', 'javascript:void(0)');
        }
    });

    inputAnimation();
    if ($('.typ-head-sec').length != 0) {
        scrollToSection();
    }
    if ($('.desktop').length != 0) {
        requestDemoScrollFix();
    }

    $(window).load(function() {
        $(".cm-loader").removeClass("active")
        if ($(".desktop").length != 0) {
            bannerEleAni()
        }
        if ($(".mobile").length != 0) {
            bannerEleAniMobile();
            btnClassChange();
        }

        if ($(".tablet.landscape").length != 0) {
            bannerEleAniTablet();
        }

        if ($(".tablet.portrait").length != 0) {
            bannerEleAniMobile();
            btnClassChange();
        }

    });

    if ($(".pg-video").length != 0) {
        videoCall();
    }

    $(".btn-down-arr").on("click", function() {
        var animatePos = $(".bs-banner").height();
        $("body,html").animate({
            scrollTop: animatePos + "px"
        }, 1000)
    })
    if ($(".bs-banner.typ-customers").length != 0) {
        slider();
    }
    if ($(".bs-banner.typ-events").length != 0) {
        slider2();
    }
    if ($(".mod-seo-content").length != 0) {
        seoSlider();
    }

    if ($('.desktop').length != 0) {
        if ($('.js-home').length != 0) {
            $('.btn-login').show();
            $('.btn-request-demo.home').addClass('fix-top');
        } else {
            $('.btn-login').hide();
            // $('.btn-request-demo.home').hide();
        }

    }

    if ($('.tablet').length != 0) {
        tabHomeBanner();
    }

    if ($('.desktop').length != 0 || $('.tablet').length != 0) {
        connectorsSameHeight();
    }

    if ($(".mobile").length != 0 || $('.tablet').length != 0) {
        if ($('.timeline-date').length != 0) {
            trimMonth();
        }
    }
});