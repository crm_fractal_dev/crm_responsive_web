<!DOCTYPE html>
<html>

<head>
    <title>Pharma</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="CRM next">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1, maximum-scale=1">
    <!-- css group start -->
    <!--#include file="modules/common/include_css.html" -->
</head>

<body class="tpl-detail">
    <h1 class="cm-not-in-page">CRMNEXT</h1>
    <!--#include file="modules/common/header.html" -->
    <!-- banner section start -->
    <div class="bs-banner typ-industries typ-pharma">
        <div class="mod-banner">
            <h2 class="banner-title">
                Pharma
            </h2>
            <p class="banner-desc">Rx for Superior Results
            </p>
        </div>
        <a class="btn btn-request-demo" href="request-demo.shtml">request demo</a>
    </div>

    <div class="lyt-industries">
        <section>
            <div class="bs-sec cm-bg typ-texture spiral typ-sm">
                <div class="sec-cont">
                    <div class="container">
                        <div class="content-wrap">
                            <h2 class="sec-title text-center">Win the field battle</h2>
                            <h3 class="sec-subtitle text-center">Capture leads from every touch point, boost conversion rates, and increase customer profitability</h3>
                            <ul class="row">
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Optimize lead </span>
                                            <span class="cm-line-break">management</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Manage all leads on a single platform, enable intelligent automated lead assignments in CRMNEXT and provide context-based coaching tips to field reps</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Personalize </span>
                                            <span class="cm-line-break">the pitch</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Leverage data within CRMNEXT, social and third-party systems to get 360-degree views of prospects and make relevant sales pitches</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Generate more</span>
                                            <span class="cm-line-break">customer value</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Create value-based relationships by segmenting customers and running focused contact programs to improve the customer portfolio quality</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="img-wrap">
                            <img src="images/industries/pharma_1.png" alt="omni channel infography">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="bs-sec cm-bg typ-default typ-sm">
                <div class="sec-cont">
                    <div class="container">
                        <div class="content-wrap">
                            <h2 class="sec-title text-center">From service to sale</h2>
                            <h3 class="sec-subtitle text-center">Stay on top of customer service requests, increase the relevance of cross sell and up sell offers, and develop a culture of self-service for standard operational requests</h3>
                            <ul class="row">
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Make it</span>
                                            <span class="cm-line-break">First-Time-Right (FTR)</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Capture &amp; manage all service requests on CRMNEXT with detailed customer histories to provide FTR resolutions and outstanding customer experiences at all touch points</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Personalize cross</span>
                                            <span class="cm-line-break">sell &amp; up sell</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Utilize portfolio data, activity patterns, and embedded analytics to help field reps offer in-context, relevant products &amp; services to customers</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Help customers</span>
                                            <span class="cm-line-break">help themselves</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Enable instant gratification to customers on mobile and web while optimizing costs with CRMNEXT Digital capabilities while reducing service costs</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="img-wrap">
                            <img src="images/industries/pharma_2.png" alt="omni channel infography">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="bs-sec cm-bg typ-texture spiral typ-sm">
                <div class="sec-cont">
                    <div class="container">
                        <div class="content-wrap">
                            <h2 class="sec-title text-center">Enable outperformance</h2>
                            <h3 class="sec-subtitle text-center">Equip the workforce with the tools to excel, drive collaboration, and track performance</h3>
                            <ul class="row">
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Equip</span>
                                            <span class="cm-line-break">to excel</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Leverage the power of CRMNEXT to upskill team members with self-paced learning options and create robust knowledge management frameworks</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Boost</span>
                                            <span class="cm-line-break">collaboration</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Enable cross-functional collaboration with in-house communication and social platforms to improve knowledge management, speed up decision-making and special approvals</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="mod-features">
                                        <h4 class="feature-title">
                                            <span class="cm-line-break">Track team</span>
                                            <span class="cm-line-break">performance</span>
                                        </h4>
                                        <div class="feature-desc">
                                            <p>Use in-built CRMNEXT capabilities to monitor team activities, set targets, ensure accountability, track achievements, and reward high performers</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="img-wrap">
                            <img src="images/industries/pharma_3.png" alt="omni channel infography">
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <section>
        <!--#include file="modules/common/mod_customer_success.html" -->
    </section>

    <!--footer section start -->
    <footer>
        <!--#include file="modules/common/footer.html" -->
    </footer>
    <!--footer section end -->
    <!--#include file="modules/common/include_js.html" -->
</body>

</html>