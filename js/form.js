// JavaScript Document
function UpdateProductName() {
            var prodObj = document.getElementById('Product');
            var prodNameObj = document.getElementById('ProductName');
            prodNameObj.value = prodObj.options[prodObj.selectedIndex].text;
        }
        function UpdateTerritoryName() {
            var terrObj = document.getElementById('Territory');
            var terrNameObj = document.getElementById('TerritoryName');
            terrNameObj.value = terrObj.options[terrObj.selectedIndex].text;
        }
        function IsValidForm() {

            var callingUrl = window.parent.document.URL;
            var errMessage = '';
            if (document.getElementById('LName').value == '') {
                errMessage = 'Enter Name.\n';
            }
            if (document.getElementById('Email').value == '') {
                errMessage += 'Enter Email.\n';
            }
            if (document.getElementById('Description').value == '') {
                errMessage += 'Enter Purpose.\n';
            }
            if (document.getElementById('Company').value == '') {
                errMessage += 'Enter Company.\n';
            }
            if (document.getElementById('Website').value == '') {
                errMessage += 'Enter Website.\n';
            }
            if (document.getElementById('Title').value == '') {
                errMessage += 'Enter Title.\n';
            }
            if (document.getElementById('Industry').value == '') {
                errMessage += 'Select Industry.\n';
            }
            if (document.getElementById('cust_364').value == '') {
                errMessage += 'Select Expected no of users.\n';
            }
            if (document.getElementById('Phone').value == '') {
                errMessage += 'Enter Phone no.\n';
            }
            if (!IsValidEmail()) {
                errMessage += 'Not a valid EmailID.\n';
            }
            if (!IsValidWebURL()) {
                errMessage += 'Not a valid WebUrl.\n';
            }
            if (errMessage != '') { alert(errMessage); return false; }
            if (errMessage != '') {
                alert(errMessage); return false;
            }
            else {             
                var rechallenge = document.getElementsByName("recaptcha_challenge_field")[0].value
                var reresponse = document.getElementsByName("recaptcha_response_field")[0].value;
                if (reresponse == "") {
                    alert("Enter verification code");
                    return false;
                }
                var VarString = "?challenge=" + rechallenge + "&response=" + reresponse
                var xhr = null;
                if (window.ActiveXObject)
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                else if (window.XMLHttpRequest)
                    xhr = new XMLHttpRequest();
                else
                    alert("not supported");

                xhr.open("GET", "http://www.crmnext.com/helper/verifycaptcha.aspx" + VarString, false);
                xhr.send(null);
                if (xhr.readyState == 4) {
                    var text = xhr.responseText;
                    var resp = text.split('\n');
                    if (resp[0] == "false") {
                        errMessage = 'Invalid verification code.\n';
                        alert(errMessage); return false;
                    }
                    else {
                        return true;
                    }
                }
                xhr = null;
            }
            return true;
           
        } function IsValidEmail() {
            if (document.getElementById('Email') == null) return true; var inem = document.getElementById("Email").value;
            if (inem != null && inem != '') {
                var filter = /^(\w+(?:\.\w+)*)@((?:\w+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                if (!filter.test(inem))
                    return false;
            } return true;
        } function IsValidWebURL() {
            return true;
            var website = document.getElementById("Website").value;
            var URL = /^((file|http):\/\/\S)*$/;
            if (!URL.test(website))
                return false;
            return true;
        }