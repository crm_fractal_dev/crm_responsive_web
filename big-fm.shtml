<!DOCTYPE html>
<html>

<head>
    <title>Customer (Bajaj Finance)</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="CRM next">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1, maximum-scale=1">
    <!-- css group start -->
    <!--#include file="modules/common/include_css.html" -->
</head>

<body class="pg-video tpl-detail">
    <h1 class="cm-not-in-page">CRMNEXT</h1>
    <!--#include file="modules/common/header.html" -->
    <!-- banner section start -->
    <div class="bs-banner typ-customers">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="mod-banner">
                        <a href="#" class="banner-logo">
                            <img src="images/leader_logo/bigfm.png" alt="bajaj logo" class="img-responsive">
                        </a>
                        <h2 class="banner-title">Propelling sales, wallet share and coverage with a unified CRM platform</h2>
                        <div class="banner-desc">
                            <p>"CRMNEXT has been able to align our thinking and approach in a single execution platform. Automation of processes and smart planning of activities have helped us to increase both, wallet share and market share."</p>
                            <p class="italic">- Tarun Katial, CEO</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <a class="cust-img-wrap typ-video-big-fm" data-toggle="modal" data-target=".customer-video-modal" href="https://www.youtube.com/embed/0OIxF9WdkVc">
                        <img src="images/customers/big_fm.jpg" alt="Bajaj Auto Finance" class="img-responsive">
                        <span class="cm-play typ-sm"></span>
                    </a>
                </div>
            </div>
        </div>
        <a class="btn btn-request-demo" href="request-demo.shtml">request demo</a>
    </div>
    <section>
        <div class="bs-sec cm-bg typ-default typ-texture abstract typ-red sec-count">
            <div class="sec-cont">
                <div class="container">
                    <ul class="row">
                        <li class="col-md-3  col-xs-6 ">
                            <div class="mod-count">
                                <h2 class="count-title">45</h2>
                                <h3 class="count-sub-title">Cities</h3>
                            </div>
                        </li>
                        <li class="col-md-3  col-xs-6 ">
                            <div class="mod-count">
                                <h2 class="count-title">1200+</h2>
                                <h3 class="count-sub-title">Towns</h3>
                            </div>
                        </li>
                        <li class="col-md-3  col-xs-6 ">
                            <div class="mod-count">
                                <h2 class="count-title">50,000+</h2>
                                <h3 class="count-sub-title">villages</h3>
                            </div>
                        </li>
                        <li class="col-md-3  col-xs-6 ">
                            <div class="mod-count">
                                <h2 class="count-title">43 Million</h2>
                                <h3 class="count-sub-title">weekly listeners</h3>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="bs-sec cm-bg typ-dusty sec-usp">
            <div class="sec-cont">
                <div class="container">
                    <ul class="row">
                        <li class="col-md-4 item">
                            <div class="mod-usp ">
                                <h2 class="usp-title typ-customer-challenge">
                                    <span class="cm-line-break">customer</span>
                                    <span class="cm-line-break">challenges</span>
                                </h2>
                                <ul class="usp-data-wrap">
                                    <li>
                                        <p class="data">Customer coverage</p>
                                        <ul class="list">
                                            <li class="sublist">Covering the entire spending spectrum with data analysis from portals like ADEX, Aircheck, etc.</li>
                                            <li class="sublist"> Creating strategies to increase its market share based on seasonality, spending power and trends of brands.</li>

                                        </ul>
                                    </li>
                                    <li>
                                        <p class="data">Activity management
                                        </p>
                                        <ul class="list">
                                            <li class="sublist">Planning and execution of effective sales activities.
                                            </li>
                                            <li class="sublist">Monitoring and executing relationship management to ensure maximum wallet share.
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p class="data">Sales forecast
                                        </p>
                                        <ul class="list">
                                            <li class="sublist">Since Ad slot is a perishable inventory, pipeline visibility is very critical. Manual reporting and coordination on excel sheets in different formats made it difficult to have a timely visibility on the opportunity
                                                pipeline.
                                            </li>
                                            <li class="sublist">Due to limited visibility and manual processes coordinated on emails and excel sheets, getting timely approvals was difficult. This would result in loss of opportunities.
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="col-md-4 item">
                            <div class="mod-usp ">
                                <h2 class="usp-title typ-solutions-provided">
                                    <span class="cm-line-break">solutions</span>
                                    <span class="cm-line-break">provided</span>
                                </h2>
                                <ul class="usp-data-wrap">
                                    <li>
                                        <p class="data">Customer Coverage</p>
                                        <ul class="list">
                                            <li class="sublist">CRMNEXT created a single version of truth by integrating with Adex, Aircheck, trafficking system- wide orbit, plum, broadview to boost the overall efficiency of the system and decision making.</li>
                                            <li class="sublist">The implementation also ensures periodic sync with Adex and Aircheck reports ensuring all new advertisers are present in the system</li>

                                        </ul>
                                    </li>
                                    <li>
                                        <p class="data">Sales Process Re-engineering
                                        </p>
                                        <ul class="list">
                                            <li class="sublist">Ensuring clarity of numbers with targets, achievements, missed opportunities, neglected customers etc. tracked on real time basis with the new system
                                            </li>
                                            <li class="sublist">Creating accountability of the sales teams with self evaluation tools for portfolio management, client coverage and wallet share.
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p class="data">Process Monitoring and Control
                                        </p>
                                        <ul class="list">
                                            <li class="sublist">Created unified processes across department for rate approvals, placement approvals, network deals etc. to improve visibility and efficiency.
                                            </li>
                                            <li class="sublist">Established strict turn-around-time and escalations based on opportunity value, ad slot inventory etc to reduce sales cycle time and better pricing power.
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="col-md-4 item">
                            <div class="mod-usp ">
                                <h2 class="usp-title typ-benefits-realized">
                                    <span class="cm-line-break">benefits</span>
                                    <span class="cm-line-break">realized</span>
                                </h2>
                                <ul class="usp-data-wrap">
                                    <li>
                                        <p class="data">Sales increased by a whopping 184% from INR 1084 million to INR 3087 million and operating profits increased from a loss of INR 328 million to INR 287.8 million.</p>
                                    </li>
                                    <li>
                                        <p class="data">
                                            Improved price discovery by defending price, focus on value selling and focus on leveraging network strength.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="data">Call rates increased, number of missed deals has declined, average sales cycle time declined resulting in improved productivity.</p>
                                    </li>
                                    <li>
                                        <p class="data">Conversion rates increased by about 40%.</p>
                                    </li>
                                    <li>
                                        <p class="data">Significant increase in repeat business and decrease in customer attrition.</p>
                                    </li>
                                    <li>
                                        <p class="data">Consistency of information and response by various teams to any customer request across organization.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <!--#include file="modules/common/mod_customer_success.html" -->
    </section>
    <!--footer section start -->
    <footer>
        <!--#include file="modules/common/footer.html" -->
    </footer>
    <!--footer section end -->
    <!--#include file="modules/common/include_js.html" -->
    <!--#include file="modules/popup/popup_video_customer.html" -->
</body>

</html>